import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PartidaService } from '../services/partida.service';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css']
})
//Esta clase controlara si es un nuevo juego u se unire a una partida existente
export class InicioComponent implements OnInit {
  
  constructor(private partida:PartidaService, private router:Router) { }

  ngOnInit(): void {
  }


  nuevoJuego(){
    this.partida.nuevoJuego().subscribe((resp:any)=>{
      this.router.navigateByUrl('tablero/'+resp)
      console.log(resp);
    })
  }

}
