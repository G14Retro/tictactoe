import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class PartidaService {
  url:string = 'https://tic-tac-toe-47319-default-rtdb.firebaseio.com/partidas';
  constructor(private http:HttpClient) { }

  nuevoJuego(){
    const datos = ({
      ganador:''
    })
    return this.http.post(this.url+'.json',datos).pipe(
      map((resp:any)=>{
        let id = resp.name;
        return id;
      })
    );
  }
}
