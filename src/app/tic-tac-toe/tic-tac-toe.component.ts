import { Component, OnInit } from '@angular/core';
import { Logica } from '../clases/logica';

@Component({
  selector: 'app-tic-tac-toe',
  templateUrl: './tic-tac-toe.component.html',
  styleUrls: ['./tic-tac-toe.component.css'],
  providers:[Logica]
})
export class TicTacToeComponent implements OnInit {

  constructor(public juego:Logica) { }

  ngOnInit(): void {
  }

}
